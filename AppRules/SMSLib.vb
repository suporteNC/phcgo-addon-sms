Public Class SMSLib
    Public Property utilizador As String = SDK.Parameters.getParameter("u6530_smssmsuser")
    Public Property password As String = SDK.Parameters.getParameter("u6530_smssmspassword")
    Public Property remetente As String = SDK.Parameters.getParameter("u6530_smssmsremetente")
    'Public Property remetenteTeste As String = "919935113"
    Public Property apiURLlogin As String = "https://ncsms.novoscanais.com/login"
    Public Property apiURLsend As String = "https://ncsms.novoscanais.com/SMSsend"
    Public Property apiURLdados As String = "https://ncsms.novoscanais.com/SMSdata"
    Public Property listMsg As New List(Of MessageVO)

    Sub New()

    End Sub

    Public Function GetToken() As String

        Dim myReq As HttpWebRequest
        Dim myResp As HttpWebResponse
        Dim reader As StreamReader
        Dim token As String
        Dim nif_empresa As String
        
        Dim query As String = "SELECT ncont FROM e1 WHERE estab = 0"
        Dim nif_result = SDK.Query.getEntityData(query)
        nif_empresa = nif_result(0).ncont

        Try
            Dim myData As String = "?username=" & utilizador & "&password=" & password & "&nif_empresa=" & nif_empresa
            myReq = HttpWebRequest.Create(apiURLlogin + myData)
            myReq.Method = "GET"
            myReq.ContentType = "application/x-www-form-urlencoded"
            myResp = myReq.GetResponse

            Dim myreader As New System.IO.StreamReader(myResp.GetResponseStream)
            Dim myText As String = myreader.ReadToEnd()

            Dim jsonObj = SDK.JSON.ToObject(Of Object)(myText)
            Dim status As String = jsonObj("status")

            If status = "OK" Then
                token = jsonObj("data")("token")
                Return token
            Else
                listMsg.add(New MsgInfo("Ocorreu um erro a autenticar na API SMS: " & jsonObj("error")))
                Return "NOK"
            End If

            Catch ex As Exception
                listMsg.add(New MsgInfo("Ocorreu um erro a autenticar na API SMS: " + ex.Message.ToString()))
                If ex.InnerException IsNot Nothing Then
                    listMsg.add(New MsgInfo(ex.InnerException.Message.ToString()))
                    SDK.Debug.LogInfo(ex.InnerException.Message.ToString())
                End If
                SDK.Debug.LogInfo("Ocorreu um erro a autenticar na API SMS: " + ex.Message.ToString())
                Return "NOK"
        End Try
    End Function

    Public Function GetSMSData() As List(Of MessageVO)
        SDK.Debug.LogInfo("A obter dados SMS.")

        Dim myReq3 As HttpWebRequest
        Dim myResp3 As HttpWebResponse
        Dim reader3 As StreamReader

        Dim token As String = GetToken()

        Try
            Dim myData3 As String = "?token=" & token
            myReq3 = HttpWebRequest.Create(apiURLdados + myData3)
            myReq3.Method = "GET"
            myReq3.ContentType = "application/x-www-form-urlencoded"

            myResp3 = myReq3.GetResponse
            Dim myreader3 As New System.IO.StreamReader(myResp3.GetResponseStream)
            Dim myText3 As String
            myText3 = myreader3.ReadToEnd()

            Dim jsonObj = SDK.JSON.ToObject(Of Object)(myText3)
            Dim status As String = jsonObj("status")
            

            If status = "OK" Then
                Dim availableCredits As String = jsonObj("data")("nr_sms")
                listMsg.add(New MsgInfo("Créditos Disponíveis: " + availableCredits))
            Else
                listMsg.add(New MsgInfo("Ocorreu um erro a obter os dados na API: " & jsonObj("error")))
            End If

            Catch ex As Exception
                SDK.Debug.LogInfo("Ocorreu um erro a obter os dados: " + ex.Message.ToString())
                listMsg.add(New MsgInfo("Ocorreu um erro a obter os dados: " + ex.Message.ToString()))

        End Try
        Return listMsg

    End Function

    Public Function GetAvailableCredits(token As String) As Integer
        SDK.Debug.LogInfo("A obter créditos disponíveis.")

        Dim myReq4 As HttpWebRequest
        Dim myResp4 As HttpWebResponse
        Dim reader4 As StreamReader

        Try
            Dim myData4 As String = "?token=" & token
            myReq4 = HttpWebRequest.Create(apiURLdados + myData4)
            myReq4.Method = "GET"
            myReq4.ContentType = "application/x-www-form-urlencoded"

            myResp4 = myReq4.GetResponse
            Dim myreader4 As New System.IO.StreamReader(myResp4.GetResponseStream)
            Dim myText4 As String
            myText4 = myreader4.ReadToEnd()

            SDK.Debug.LogInfo(myText4)

            Dim jsonObj = SDK.JSON.ToObject(Of Object)(myText4)
            Dim status As String = jsonObj("status")
            

            If status = "OK" Then
                Dim availableCredits As String = jsonObj("data")("nr_sms")
                Dim availableCreditsNumber As Integer = Convert.toInt32(availableCredits)
                return availableCreditsNumber
            Else
                return -1
            End If

            Catch ex As Exception
                SDK.Debug.LogInfo("Ocorreu um erro a obter os dados: " + ex.Message.ToString())
                return -1

        End Try
    End Function

    Public Function SendSMS(numero As String, mensagem As String) As List(Of MessageVO)

        SDK.Debug.LogInfo("A enviar SMS.")
        Dim myReq2 As HttpWebRequest
        Dim myResp2 As HttpWebResponse
        Dim reader2 As StreamReader
        Dim token As String = GetToken()
        Dim availableCreditsNumber As Integer = GetAvailableCredits(token)
        Dim minimumBalanceNumber = SDK.Parameters.getParameter("u6530_smssmsminimumbalance")

        If availableCreditsNumber <> -1 Then
            If availableCreditsNumber <= minimumBalanceNumber Then
                'listMsg.add(New MsgInfo("O saldo de SMS está abaixo do valor mínimo."))
                Dim noticeBiz as SDKBiz = SDK.Business.CreateBiz(Notice.getEntityName)
                Dim noticePhcResult As PHCResult = noticeBiz .GetNewInstance()
                Dim newNotice As NoticeVO = noticePhcResult.GetResult(Of NoticeVO)()
                newNotice.titlept = "SMS"
                newNotice.titlees = "SMS"
                newNotice.titleen = "SMS"
                newNotice.textpt = "O saldo de SMS está abaixo do valor mínimo. Tem " & availableCreditsNumber.ToString() & " créditos disponíveis. Visite https://msgbox.pt para carregar."
                newNotice.textes = "El saldo de SMS está por debajo del monto mínimo. Tienes " & availableCreditsNumber.ToString() & " créditos disponibles. Visite https://msgbox.pt para cargar."
                newNotice.texten = "The SMS balance is below the minimum amount. You have " & availableCreditsNumber.ToString() & " credits available. Visit https://msgbox.pt to top-up."
                newNotice.type = NoticeType.Warning
                newNotice.targetUrl = "https://msgbox.pt"
                newNotice.targetUser = ""
                listMsg.AddRange(noticeBiz.Save(newNotice))
            End If
        End If

        

        Try
            myReq2 = HttpWebRequest.Create(apiURLsend)
            myReq2.Method = "POST"
            myReq2.ContentType = "application/x-www-form-urlencoded"

            Dim myData2 As String = "token=" & token & "&alfaSender=" & remetente & "&phoneNumber=" & numero & "&messageText=" & HttpUtility.UrlEncode(mensagem)

            myReq2.GetRequestStream.Write(System.Text.Encoding.UTF8.GetBytes(myData2), 0, System.Text.Encoding.UTF8.GetBytes(myData2).Count)
            myResp2 = myReq2.GetResponse
            Dim myreader2 As New System.IO.StreamReader(myResp2.GetResponseStream)
            Dim myText2 As String
            myText2 = myreader2.ReadToEnd()

            Dim jsonObj = SDK.JSON.ToObject(Of Object)(myText2)
            Dim status As String = jsonObj("status")
            
            If status = "OK" Then
                Dim result As String = jsonObj("data")("Result")
                If result = "OK" Then
                    listMsg.add(New MsgInfo("SMS enviada: " + mensagem + " Destinatário: " + numero))
                Else
                    listMsg.add(New MsgInfo("Ocorreu um erro ao enviar SMS: NOT OK."))
                End If
            Else
                listMsg.add(New MsgInfo("Ocorreu um erro ao enviar SMS: NOT OK."))
            End If

            Catch ex As Exception
                SDK.Debug.LogInfo("Ocorreu um erro ao enviar SMS: " + ex.Message.ToString())
                listMsg.add(New MsgInfo("Ocorreu um erro ao enviar SMS: " + ex.Message.ToString()))
        End Try
        Return listMsg

    End Function
End Class